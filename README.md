
## T5 Transformer Model for Generating Titles from Job Postings

**Introduction**

T5 model is a Sequence-to-Sequence model. A Sequence-to-Sequence model is fully capable to perform any text to text conversion task. What does it mean? - It means that a T5 model can take any input text and convert it into any output text. Such text-to-text conversion is useful in NLP tasks like language translation, summarization, text generation etc.

For this project We will take job posting text or job descriptions as our input text and its corresponding job title or job profile as output text and feed it to a T5 model to train. Once the model is trained, it will be able to generate the job title based on the job posting.

**Data**

We will use its job posting and title columns to train our model.

- title: This column represents the title of the job posting
- job_posting_text: This column represents the content of the job posting.

This will be a supervised training where job posting text is our independent variable X while title is our dependent variable y.

**How to run**

```
%run predict.py
```  

Let's load a job posting:

```Python
>> posting = get_posting('data/job_p.txt')
>> predicted_title = predict_title(model_loaded, posting, 3, 10, 3.0)
>> print(predicted_title)


[['Accounts Payable Specialist', 'Accounts Payable Specialist In', 'Completions Payable Specialist']]


```  
