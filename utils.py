'''
#STOPWORDS = ['of', 'and', 'in', 'for']
#STOPWORDS = set(STOPWORDS)

magpie = Magpie(
         keras_model='model/bid_bid_lstm_gaussian_att_bn_new_set_200.h5',
         word2vec_model='embedding/complete_embeddings_new',
         scaler='stand_scaler/complete_scaler_new.pickle',
         labels = labels)
'''

import functools
import time
import nltk
from nltk.corpus import stopwords
import geonamescache
import numpy as np
import pandas as pd


def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer


# set stopwords
stop_words = set(stopwords.words('english'))
stop_words.remove('it')
stop_words.remove('ma')
stop_words.remove('i')
stop_words.remove('is')
stop_words.remove('re')
stop_words.add('available')
stop_words.add('near')
stop_words.add('west')
stop_words.add('east')
stop_words.add('located')
stop_words.add('per')
stop_words.add('week')
stop_words.add('day')
stop_words.add('days')
stop_words.add('week-')
stop_words.add('immediate')
stop_words.add('annually')
stop_words.add('weekly')
stop_words.add('daily')
# stop_words.add('prominent')
stop_words.add('midwest')
stop_words.add('nyc')
stop_words.add('openings')
stop_words.add('opening')
stop_words.add('downtown')
stop_words.add('must')
stop_words.add('hours')
stop_words.add('hour')
stop_words.add('perm')
# stop_words.add('growing')
stop_words.add('based')
# stop_words.add('fast')
stop_words.add('permanent')
stop_words.add('local')
stop_words.add('required')
stop_words.add('bonus')
# stop_words.add('beach')
stop_words.add('needed')
stop_words.add('need')
stop_words.add('needs')
# stop_words.add('il')
stop_words.add('southside')
stop_words.add('inc')
stop_words.add('uk')
stop_words.add('monday')
stop_words.add('friday')

# list of US cities, US states and World Countries to remove
gc = geonamescache.GeonamesCache()
c = gc.get_cities()
US_cities = [c[key]['name'] for key in list(c.keys())
             if c[key]['countrycode'] == 'US']
US_cities = [x.lower() for x in US_cities]
c = gc.get_us_states()
US_states = [c[key]['name'] for key in list(c.keys())]
US_states = [x.lower() for x in US_states]
countries = gc.get_countries_by_names()
countries_list = list(countries.keys())
WORLD_COUNTRIES = [x.lower() for x in countries_list]
stop_words.update(US_cities)
stop_words.update(US_states)
stop_words.update(WORLD_COUNTRIES)
stop_words.remove('enterprise')
stop_words.remove('mobile')
############################
STOPWORDS = stop_words
#STOPWORDS = ['of', 'and', 'in', 'for']
#STOPWORDS = set(STOPWORDS)
