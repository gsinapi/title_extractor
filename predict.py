# -*- coding: utf-8 -*-
"""
Created on Sun May  3 11:09:17 2020
t5m environment
https://shivanandroy.com/transformers-generating-arxiv-papers-title-from-abstracts/
https://huggingface.co/blog/how-to-generate

Best predictions (thirdmodel-epoch-4 ):
model_loaded.args.repetition_penalty = 3.0 (find repetitions)
-predict_title(model_loaded_new, text_job, 3, 3)
-predict_title(model_loaded_new, text_job, 3, 5)
-predict_title(model_loaded_new, text_job, 3, 10)
-predict_title(model_loaded_new, text_job, 3, 15)
-predict_title(model_loaded_new, text_job, 3, 50)
-predict_title(model_loaded_new, text_job, 3, 100)


- secondmodel-epoch-4: all dataset, 0.2 train/test/, 20 lenght, beams 10, 4 epochs. eval_loss: 0.33
- thirdmodel-epoch-4: all dataset, 0.1 train/test/, 20 lenght, 10 epochs, no beam BEST
- fourth model: all dataset, 0.1 train/test/, 200 lenght, 5 epochs, no beam BEST

Best predictions (catastrophe, Transactional, Actuary, Broking, Advisory):
- thirdmodel-epoch-4, 3.0 penalty, 3/50 num_beams
- fourthmodel-epoch-4, 3.0 penalty, 10/3/50 num_beams

Gold set:
- MMC
- Duke
_ job Postings

"""
import pandas as pd
import numpy as np
import pyarrow.feather as pf
import pyarrow.parquet as pq
import logging
# import torch
from simpletransformers.t5 import T5Model
logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)


def get_posting(input_path):
    with open(input_path, encoding='cp1252') as f:  # encoding="utf-8"
        list_titles = f.read()
    return list_titles


def predict_title(model, posting, top_n=5, num_beams=20, rep_penalty=1.0, sampling=False):
    '''
    - for models trained (thirdmodel-epoch-4, thirdmodel-epoch-10) wiht max seq length = 20, if len(posting.split) <=20, then max_seq_length = 200
    - for models trained (fourthmodel-epoch) with max seq lenght = 200, don't modify max seq lenght
    '''
    actual_abstract = ["summarize: "+posting]
    #model.args.max_seq_length = 200
    ###############################
    model.args.max_seq_length = min(len(posting.split()), 200)
    if model.args.max_seq_length <= 30:
        model.args.max_seq_length = 200
    #####################################
    model.args.repetition_penalty = rep_penalty
    model.args.num_return_sequences = int(top_n)
    model.args.num_beams = int(num_beams)
    model.args.do_sample = sampling
    predicted_title = model.predict(actual_abstract)
    return predicted_title


def predict_title_sample(model_loaded, posting, top_k, top_p, top_n=5):
    model_args = {
        "reprocess_input_data": True,
        "overwrite_output_dir": True,
        "max_seq_length": 256,
        "eval_batch_size": 128,
        "num_train_epochs": 1,
        "save_eval_checkpoints": False,
        "use_multiprocessing": False,
        "num_beams": None,
        "do_sample": True,
        "max_length": 50,
        "top_k": 50,
        "top_p": 0.95,
        "num_return_sequences": 3,
        "manual_seed": 4
    }
    # model_loaded = T5Model(
    #     "t5", "outputs/fifthmodel-epoch-2", args=model_args, use_cuda=False
    # )
    model_loaded = T5Model(
        "t5", "outputs/fifthmodel-epoch-4", args=model_args_pred, use_cuda=False
    )
    model_loaded.args.manual_seed = 4
    model_loaded.args.top_k = top_k
    model_loaded.args.top_p = top_p
    model_loaded.args.num_beams = None
    model_loaded.args.max_seq_length = min(len(posting.split()), 200)
    model_loaded.args.do_sample = True
    model_loaded.args.num_return_sequences = int(top_n)
    if model_loaded.args.max_seq_length <= 30:
        model_loaded.args.max_seq_length = 200
    actual_abstract = ["summarize: "+posting]
    predicted_title = model_loaded.predict(actual_abstract)
    return predicted_title


if __name__ == '__main__':
    model_args_pred = {
        "reprocess_input_data": True,
        "overwrite_output_dir": True,
        "max_seq_length": 200,
        "train_batch_size": 64,
        "num_train_epochs": 4,
        "num_beams": 10,
        "num_return_sequences": 5
    }

    model_loaded = T5Model(
        "t5", "outputs/fourthmodel-epoch-4", args=model_args_pred, use_cuda=False
    )

    # model_loaded = T5Model(
    #     "t5", "outputs/checkpoint-100000", args=model_args_pred, use_cuda=False
    # )

    ###############################################
    # posting = get_posting('data/269442.txt')
    # actual_abstract = ["summarize: "+posting]
    # model_loaded.args.max_seq_length = min(len(posting.split()), 200)
    # #model_loaded.args.max_seq_length = len(posting.split())
    # model_loaded.args.num_return_sequences = 5
    # model_loaded.args.num_beams = 20
    # predicted_title = model_loaded.predict(actual_abstract)
    ############# predictor #################################
    # posting = get_posting('data/269443.txt')
    # model_loaded.args.top_k = 50
    # model_loaded.args.top_p = 0.95
    # model_loaded.args.num_beams = None
    # model_loaded.args.max_seq_length = min(len(posting.split()), 200)
    # model_loaded.args.do_sample = True
    # actual_abstract = ["summarize: "+posting]
    # model_loaded.predict(actual_abstract)

    ##################################################################

    posting = get_posting('data/job_p.txt')
    predicted_title = predict_title(model_loaded, posting, 3, 10, 3.0)
    #predicted_title_sample = predict_title_sample(model_loaded, posting, 50, 0.95, 5)
    ##########################################################
    # data = pd.read_excel('data/MMC_Job_Catalog Sept 2018 - formatted.xlsx')
    # df = pd.DataFrame()
    # for i in range(len(data)):
    #     jp = data['Job Profile'][i]
    #     text_job = str(data['Job Profile Summary'][i])
    #     #text_job = str(data['Job Profile'][i]) + ' ' + str(data['Job Profile Summary'][i])
    #     #text_job = str(data['Job Profile Description'][i])
    #     print(jp, '-----', predict_title(model_loaded, text_job, 3, 10, 3.0))
    #     df = df.append({'Job Profile': jp, 'Job Profile Summary': text_job,
    #                     'title_predictions_5_rep': predict_title(model_loaded, text_job, 3, 5, 3.0),
    #                     'title_predictions_5': predict_title(model_loaded, text_job, 3, 5, 1.0),
    #                     'title_predictions_3_rep': predict_title(model_loaded, text_job, 3, 3, 3.0),
    #                     'title_predictions_3': predict_title(model_loaded, text_job, 3, 3, 1.0),
    #                     'title_predictions_10_rep': predict_title(model_loaded, text_job, 3, 10, 3.0),
    #                     'title_predictions_10': predict_title(model_loaded, text_job, 3, 10, 1.0),
    #                     'title_predictions_50_rep': predict_title(model_loaded, text_job, 3, 50, 3.0),
    #                     'title_predictions_50': predict_title(model_loaded, text_job, 3, 50, 1.0)},
    #                    ignore_index=True)
    # df.to_csv('data/results_t5_fourthmodel4ep_mmc.csv', index=False)
