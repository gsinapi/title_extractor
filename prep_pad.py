# -*- coding: utf-8 -*-
"""
Created on Sun May  3 11:09:17 2020
t5m environment
https://shivanandroy.com/transformers-generating-arxiv-papers-title-from-abstracts/
https://github.com/safakkbilici/Academic-Paper-Title-Recommendation/tree/f4ee6ba034e2b72eae35a5e8c0133c60e39ad5ec/T5
"""
import pandas as pd
import numpy as np
import pyarrow.feather as pf
import pyarrow.parquet as pq
import logging
from simpletransformers.t5 import T5Model
logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)


def get_posting(input_path):
    with open(input_path, encoding='cp1252') as f:  # encoding="utf-8"
        list_titles = f.read()
    return list_titles


class TitleDataset:
    '''
    add urlify
    '''

    def __init__(self, vocab_size, encoder_time_steps, decoder_time_steps):
        self.dic_token_index = None
        self.dic_index_token = None
        title = []
        text = []
        #######################
        dic = pq.read_table('data/text_title_subocc_final.parquet').to_pandas()
        dic = dic.sample(frac=1).reset_index(drop=True)
        #dic = dic[:1000]
        parser = urlify()
        #dic['clean_title'] = dic['clean_title'].fillna('').astype(str).apply(parser)
        #dic['job_posting_text'] = dic['job_posting_text'].fillna('').astype(str).apply(parser)
        dic['clean_title'] = dic['clean_title'].fillna('').astype(str)
        dic['job_posting_text'] = dic['job_posting_text'].fillna('').astype(str)
        for i in range(len(dic)):
            print(i)
            title.append('<start> '+parser(dic['clean_title'][i]) + ' <end>')
            text.append(parser(dic['job_posting_text'][i]))
        ###############################################
        #data.to_json('temp.json', orient='records', lines=True)
        #################################################
        # with open('title.json') as f:
        #     for line in f.readlines():
        #         dic = json.loads(line)
        #         title.append('<start> '+dic['Title']+' <end>')
        #         text.append(dic['Abstract'])
        ##########################################
        t = Tokenizer(num_words=vocab_size,
                      oov_token=None,
                      filters='!"#$%&()*+,-./:;=?@[\\]^_`{|}~\t\n',)
        t.fit_on_texts(text+title)
        with open('word_tokenizer.pickle', 'wb') as handle:
            pickle.dump(t, handle, protocol=pickle.HIGHEST_PROTOCOL)
        self.dic_token_index = t.word_index
        self.dic_index_token = {t.word_index[item]: item for item in t.word_index}

        encoded_texts = t.texts_to_sequences(text)
        encoded_titles = t.texts_to_sequences(title)

        encoded_inputs = []
        encoded_targets = []

        for item in encoded_titles:
            encoded_inputs.append(item[0:(len(item)-1)])
            encoded_targets.append(item[1:(len(item))])

        encoder_input = pad_sequences(encoded_texts,
                                      maxlen=encoder_time_steps,
                                      padding='post')
        decoder_input = pad_sequences(encoded_inputs,
                                      maxlen=decoder_time_steps,
                                      padding='post')
        decoder_target = pad_sequences(encoded_targets,
                                       maxlen=decoder_time_steps,
                                       padding='post')
        # Randomly shuffle data
        np.random.seed(10)
        shuffle_indices = np.random.permutation(np.arange(len(encoder_input)))
        encoder_input_shuffled = encoder_input[shuffle_indices]
        decoder_input_shuffled = decoder_input[shuffle_indices]
        decoder_target_shuffled = decoder_target[shuffle_indices]

        # Split train/test set
        self.encoder_input_train = encoder_input_shuffled[:-100000]
        self.encoder_input_dev = encoder_input_shuffled[-100000:]
        self.decoder_input_train = decoder_input_shuffled[:-100000]
        self.decoder_input_dev = decoder_input_shuffled[-100000:]
        self.decoder_target_train = decoder_target_shuffled[:-100000]
        self.decoder_target_dev = decoder_target_shuffled[-100000:]


#dataset = TitleDataset(hp.vocab_size, hp.encoder_time_steps, hp.decoder_time_steps)
# with open('titledataset.pkl', 'wb') as f:
#    pickle.dump(dataset, f)
if __name__ == '__main__':
    #prediction = True
    dic = pq.read_table('data/text_title_subocc_final.parquet').to_pandas()
    dic = dic.sample(frac=1).reset_index(drop=True)
    #dic = dic[:100000]
    dic['clean_title'] = dic['clean_title'].fillna('').astype(str)
    dic['job_posting_text'] = dic['job_posting_text'].fillna('').astype(str)
    dic = dic[['clean_title', 'job_posting_text']]
    #dic.columns - ['title', 'abstract']
    papers = dic.copy()
    papers.columns = ['target_text', 'input_text']
    papers = papers.dropna()
    eval_df = papers.sample(frac=0.10, random_state=101)  # 0.20
    train_df = papers.drop(eval_df.index)

    train_df['prefix'] = "summarize"
    eval_df['prefix'] = "summarize"
    eval_df = eval_df.reset_index(drop=True)

    # second model
    # model_args = {
    #     "reprocess_input_data": True,
    #     "overwrite_output_dir": True,
    #     "max_seq_length": 20,  # 20, 200
    #     "train_batch_size": 64,
    #     "num_train_epochs": 10,
    #     "num_beams": 10,
    #     "num_return_sequences": 5
    # }
    # third model
    # model_args = {
    #     "reprocess_input_data": True,
    #     "overwrite_output_dir": True,
    #     "max_seq_length": 20,  # 20, 200
    #     "train_batch_size": 64,
    #     "num_train_epochs": 10
    # }
    # print(model_args)
    # fourth model
    # model_args = {
    #     "reprocess_input_data": True,
    #     "overwrite_output_dir": True,
    #     "max_seq_length": 200,
    #     "train_batch_size": 64,
    #     "num_train_epochs": 5
    # }
    # fifth model
    # model_args = {
    #     "reprocess_input_data": True,
    #     "overwrite_output_dir": True,
    #     "max_seq_length": 200,
    #     "train_batch_size": 8,
    #     "num_train_epochs": 5,
    #     "evaluate_during_training": True,
    #     "evaluate_during_training_steps": 50000,
    #     "evaluate_during_training_verbose": True
    # }
    # sixth model
    model_args = {
        "reprocess_input_data": True,
        "overwrite_output_dir": True,
        "max_seq_length": 256,
        "train_batch_size": 2,
        "num_train_epochs": 3,
        "evaluate_during_training": True,
        "evaluate_during_training_steps": 100000,
        "evaluate_during_training_verbose": True,
        "save_eval_checkpoints": True,
        "save_steps": -1
    }
    print(model_args)

    model = T5Model("t5", "t5-small", args=model_args, use_cuda=False)
    # model_loaded = T5Model(
    #    "t5", "outputs/checkpoint-1250-epoch-1", use_cuda=False
    # )

    # Train T5 Model on new task
    model.train_model(train_df, eval_data=eval_df)
    # model.train_model(train_df)

    # Evaluate T5 Model on new task
    results = model.eval_model(eval_df)
    # if prediction:
    #     random_num = 350
    #     actual_title = eval_df.iloc[random_num]['target_text']
    #     actual_abstract = ["summarize: "+eval_df.iloc[random_num]['input_text']]
    #     predicted_title = model.predict(actual_abstract)
    #
    #     print(f'Actual Title: {actual_title}')
    #     print(f'Predicted Title: {predicted_title}')
    #     print(f'Actual Abstract: {actual_abstract}')
    ################## predict ##############################
    # model_args_pred = {
    #     "reprocess_input_data": True,
    #     "overwrite_output_dir": True,
    #     "max_seq_length": 200,
    #     "train_batch_size": 64,
    #     "num_train_epochs": 4,
    #     "num_beams": 10,
    #     "num_return_sequences": 5
    # }
    # model_loaded = T5Model(
    #     "t5", "outputs/first_model_100k_1ep", args=model_args_pred, use_cuda=False
    # )
    # posting = get_posting('data/269442.txt')
    # actual_abstract = ["summarize: "+posting]
    # model_loaded.args.max_seq_length = min(len(posting.split()), 200)
    # model_loaded.args.max_seq_length = len(posting.split())
    # model_loaded.args.num_return_sequences = 5
    # predicted_title = model_loaded.predict(actual_abstract)
