import logging
import re
from utils import STOPWORDS


class urlify(object):

    """
    Preprocess and tokenize text.

    Example:
        input: "A/P A/B Full-Stack Analyst - M&A c/c#/c++ 2nd shift at 12334 In $100K + bonus Downtown Boston"
        output: "a/p a/b full-stack analyst m&a c/c#/c++ 2nd shift"
    """

    def __init__(
        self,
        list_acr=None,
        stopwords=None
    ):
        """
        Initialize the acronym list and stopwords
        Args:
            list_acr: A list of acronyms or special characters to not split or tokenize
            stopwords: a list of stopwords
        """
        # self.list_acr = ['M&A', 'P&C', 'FP&A', 'H&B', 'R&D', 'L&D',
        #                 'F&B', 'I&C', 'O&M', 'F&I', 'I&E', 'L&I', 'E&I', 'Pm&R', 'C&A', 'A/C', 'E/O', 'A/V', 'A/B', 'A/C'
        #                 'E-Commerce', 'Co-op', 'On-', 'In-', 'Pre-', 'post-', 'Full-Stack', 'e-', 'co-',
        #                 '.Net', 'c#', 'c++', 'k-', 'business-to', 'f#', 'cdl-', 'high-']

        self.list_acr = ['M&A', 'P&C', 'FP&A', 'H&B', 'R&D', 'L&D',
                         'F&B', 'I&C', 'O&M', 'F&I', 'I&E', 'L&I', 'E&I', 'Pm&R', 'C&A', 'A/C', 'E/O', 'A/V', 'A/B', 'A/C'
                         'E-Commerce', 'Co-op', 'On-', 'In-', 'Full-Stack', 'e-', 'co-',
                         '.Net', 'c#', 'c++', 'k-', 'business-to', 'f#', 'cdl-', 'a&p']
        self.list_acr = [item.lower() for item in self.list_acr]
        self.stop_words = STOPWORDS

    def __call__(self, s):
        """Custom tokenizer

        It handles abbreviations, acronyms, special symbols.

        Parameters
        ----------
        s: str
           Input string to preprocess

        Returns
        -------
        final sentence or s: str
            Preprocessed string

        """
        s = s.lower()
        s = re.sub(r"\b12\b", "12th", s)
        s = re.sub(r"\b10\b", "10th", s)

        s = ' '.join([word for word in s.split() if not any(
            [phrase in word for phrase in ['$']])])

        if any(ext in s for ext in self.list_acr):
            s = s.lower()
            tokens = s.split()
            final_list_tokens = []
            for token in tokens:
                if any(ext in token for ext in self.list_acr):
                    final_list_tokens.append(token)
                else:
                    list_spec = ['&', '/']
                    if any(ext in token for ext in list_spec):
                        special_char = [
                            ext for ext in list_spec if(ext in token)][0]
                        if (any(ext in special_char for ext in ['&', '/']) and (len(token.split(special_char)[0]) == 1 and len(token.split(special_char)[1]) == 1)):
                            final_list_tokens.append(token)
                        else:
                            token = token.lower()
                            token = token.replace("/", " ")
                            token = token.replace("-", " ")
                            token = token.replace("&", " ")
                            token = token.replace("_", " ")
                            token = token.replace(",", " ")
                            token = re.sub(r"[^\w\s]", '', token)
                            token = re.sub(r"\b\d+\b", "", token)
                            final_list_tokens.append(token)
                    else:
                        token = token.lower()
                        token = token.replace("/", " ")
                        token = token.replace("-", " ")
                        token = token.replace("&", " ")
                        token = token.replace("_", " ")
                        token = token.replace(",", " ")
                        token = re.sub(r"[^\w\s]", '', token)
                        token = re.sub(r"\b\d+\b", "", token)
                        final_list_tokens.append(token)
            final_sentence = ' '.join(final_list_tokens)
            final_sentence = ' '.join(
                [word for word in final_sentence.split() if word not in self.stop_words])
            # Replace all runs of whitespace with a single dash
            final_sentence = re.sub(r"\s+", ' ', final_sentence)
            return final_sentence
        else:
            list_spec = ['&', '/']
            if any(ext in s for ext in list_spec):
                s = s.lower()
                tokens = s.split()
                final_list_tokens = []
                for token in tokens:
                    if any(ext in token for ext in list_spec):
                        special_char = [
                            ext for ext in list_spec if(ext in token)][0]
                        if (any(ext in special_char for ext in ['&', '/']) and (len(token.split(special_char)[0]) == 1 and len(token.split(special_char)[1]) == 1)):
                            final_list_tokens.append(token)
                        else:
                            token = token.lower()
                            token = token.replace("/", " ")
                            token = token.replace("-", " ")
                            token = token.replace("&", " ")
                            token = token.replace("_", " ")
                            token = token.replace(",", " ")
                            # Remove all non-word characters (everything except numbers and letters)
                            token = re.sub(r"[^\w\s]", '', token)
                            token = re.sub(r"\b\d+\b", "", token)
                            final_list_tokens.append(token)
                    else:
                        token = token.lower()
                        token = token.replace("/", " ")
                        token = token.replace("-", " ")
                        token = token.replace("&", " ")
                        token = token.replace("_", " ")
                        token = token.replace(",", " ")
                        # Remove all non-word characters (everything except numbers and letters)
                        token = re.sub(r"[^\w\s]", '', token)
                        token = re.sub(r"\b\d+\b", "", token)
                        final_list_tokens.append(token)

                final_sentence = ' '.join(final_list_tokens)
                final_sentence = ' '.join(
                    [word for word in final_sentence.split() if word not in self.stop_words])
                # Replace all runs of whitespace with a single dash
                final_sentence = re.sub(r"\s+", ' ', final_sentence)
                return final_sentence

            else:
                s = s.lower()
                s = s.replace("/", " ")
                s = s.replace("-", " ")
                s = s.replace("&", " ")
                s = s.replace("_", " ")
                s = s.replace(",", " ")
                # Remove all non-word characters (everything except numbers and letters)
                s = re.sub(r"[^\w\s]", '', s)
                s = re.sub(r"\b\d+\b", "", s)
                s = ' '.join([word for word in s.split()
                              if word not in self.stop_words])
                # Replace all runs of whitespace with a single dash
                s = re.sub(r"\s+", ' ', s)
                return s
